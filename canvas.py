import math


class Canvas(object):
    def __init__(self, size, camera=(0, 0)):
        self.items = []

        self.size = CanvasVector(size)
        self.camera = CanvasVector(camera)
        self.back_buffer = ''

    def move_camera(self, vec):
        self.camera += CanvasVector(vec)

    def move_camera_to(self, vec):
        self.camera = CanvasVector(vec)

    def __repr__(self):
        return "{}<{}x{}>({} items)".format(type(self).__name__, self.size.x, self.size.y, len(self))

    def __len__(self):
        return len(self.items)

    def add(self, *items):
        for item in items:
            if isinstance(item, CanvasObject):
                self.items.append(item)

            else:
                raise TypeError("{} is not a CanvasObject instance".format(repr(item)))

    def remove(self, *items):
        for item in items:
            self.items = [x for x in self.items if x is not item]

    def render(self):
        targ = CanvasTarget(self, ' ')
        pen = CanvasPen(targ)

        max_iters = 5000
        i = 0

        interm = list(self.items)

        while len(interm) > 0 and i <= max_iters:
            it = interm.pop(0)

            if isinstance(it, (list, set, tuple)):
                interm = list(it) + interm

            elif isinstance(it, CanvasObject):
                it.canvas = self
                res = it.render(self, pen)
                it.canvas = None

                if res:
                    interm = list(res) + interm

                i += 1

            elif hasattr(it, '__call__'):
                res = it(self, pen)

                if res:
                    interm = list(res) + interm
                
                i += 1

            else:
                raise TypeError("Tried to render unknown object: {}".format(repr(it)))

        if len(interm) > 0:
            raise RuntimeError("Intermediary element rendering iteration overflow (over 5000 iterations detected)")

        self.back_buffer = ''
        self.print('\x1B[?25l')
        self.topleft()

        for y in range(self.size.y):
            r = ''.join(targ.row(y)).replace('\n', '') + '\n'
            self.print(r)

        self.swap()

    def swap(self):
        print(self.back_buffer, end='')

    def show_cursor(self):
        self.print('\x1B[?25h')

    def clear(self):
        self.print('\x1B[2J') # clear display
        self.topleft()

    def print(self, data):
        self.back_buffer += data

    def topleft(self):
        self.print('\x1B[H') # cursor to the top left

    def __del__(self):
        #self.clear()
        pass

class CanvasObject(object):
    canvas = None

    def render(self, canvas, pen):
        raise NotImplementedError("{}.render".format(type(self).__name__))

class OverlayObject(CanvasObject):
    def render(self, canvas, pen):
        cam = canvas.camera
        canvas.camera = CanvasVector((canvas.size.x / 2, canvas.size.y / 2))

        self.overlay_render(canvas, pen)

        canvas.camera = cam

    def overlay_render(self, canvas, pen):
        raise NotImplementedError("{}.overlay_Render".format(type(self).__name__))

class CanvasVector(object):
    def __init__(self, vec):
        if isinstance(vec, (list, tuple)):
            self.x = vec[0]
            self.y = vec[1]

        elif hasattr(vec, 'x') and hasattr(vec, 'y'):
            self.x = vec.x
            self.y = vec.y

        else:
            raise ValueError("Non-vectorial value: {}".format(repr(vec)))

    def size(self):    
        l = math.sqrt(float(self.x * self.x) + float(self.y * self.y))
        return l

    def unit(self):
        if self.size() == 0:
            return CanvasVector((0, 0))

        return self / self.size()

    def __add__(self, b):
        return type(self)((self.x + b.x, self.y + b.y))

    def __neg__(self):
        return type(self)((-self.x, -self.y))

    def __mul__(self, b):
        if isinstance(b, type(self)):
            bv = type(self)(b)
            return type(self)((self.x * bv.x, self.y * bv.y))

        else:
            return type(self)((self.x * b, self.y * b))

    def __truediv__(self, b):
        if isinstance(b, type(self)):
            bv = type(self)(b)
            return type(self)((self.x / bv.x, self.y / bv.y))

        else:
            return type(self)((self.x / b, self.y / b))

    def dot(self, b):
        assert isinstance(b, type(self))
        return self.x * b.x + self.y * b.y

    def __sub__(self, b):
        return self + (-b)

    def __repr__(self):
        return "{}(x={},y={})".format(type(self).__name__, self.x, self.y)

class CanvasPen(object):
    def __init__(self, target):
        self.target = target
        self.pos = CanvasVector((0, 0))
        self.alpha = 1
        self.quant_error = 0
        self.angle = 0

    def aim(self, angle):
        self.angle = angle

    def turn_right(self, angle_offset = 90):
        self.angle -= angle_offset

    def turn_left(self, angle_offset = 90):
        self.angle += angle_offset

    def forward(self, length, char = ''):
        offset = CanvasVector((
            math.cos(self.angle) * length,
            math.sin(self.angle) * length
        ))

        if char:
            self.line_toward(offset, char)

        else:
            self.move(offset)

    def set_alpha(self, alpha):
        self.alpha = alpha
        self.quant_error = 0

    def reset_alpha(self):
        self.set_alpha(1)
    
    def move_to(self, pos):
        self.pos = CanvasVector(pos)

    def move(self, offs):
        self.pos += CanvasVector(offs)

    def line_toward(self, offs, char):
        self.line_to(self.pos + CanvasVector(offs), char)

    def line_to(self, pos2, char):
        pos = self.pos
        pos2 = CanvasVector(pos2)

        if pos.x == pos2.x and pos.y == pos2.y:
            self.put(pos.x, pos.y, char)
            return

        self.pos = pos2

        if pos.x > pos2.x:
            self.move_to(pos2)
            self.line_to(pos, char)
            self.move_to(pos2)

        offs = pos2 - pos

        if offs.y == 0:
            for x in range(int(pos.x), int(pos2.x) + 1):
                self.put(x, pos.y, char)

        elif offs.x == 0:
            y0 = int(min(pos.y, pos2.y))
            y1 = int(max(pos.y, pos2.y))

            for y in range(y0, y1 + 1):
                self.put(pos.x, y, char)
        
        else:
            slope = offs.y / offs.x
            
            if abs(slope) <= 1:
                for x in range(int(pos.x), int(pos2.x) + 1):
                    real_y = pos.y + (x - pos.x) * slope
                    y = int(real_y)
                    self.put(x, y, char)
            
            else:
                y1 = int(pos.y)
                y2 = int(pos2.y)

                for y in range(min(y1, y2), max(y1, y2) + 1):
                    real_x = pos.x + (y - pos.y) * (1 / slope)
                    x = int(real_x)
                    self.put(x, y, char)

                
    def write(self, text):
        x = self.pos.x
        y = self.pos.y

        self.pos += CanvasVector((len(text), 0))

        for l in text:            
            self.put(x, y, l)
            x += 1

    def arc(self, radius, angle = math.pi * 2, char = ''):
        # Adapted from Turtle code.
        arc_length = radius * abs(angle)
        num_steps = int(arc_length / 3) + 1
        step_length = arc_length / num_steps
        step_angle = float(angle) / num_steps

        self.turn_left(step_angle / 2)

        for _ in range(num_steps):
            self.forward(step_length, char)
            self.turn_left(step_angle)

        self.turn_right(step_angle / 2)

    def rect(self, size, char, pos = None):
        alpha = self.alpha
        self.alpha = 1

        pos = pos or self.pos

        if alpha > 0:
            if alpha == 1:
                for x in range(pos.x, pos.x + size.x):
                    for y in range(pos.y, pos.y + size.y):
                        self.put(x, y, char)

            else:
                values = [alpha for _ in range(size.x * size.y)]

                def add_safe(idx, val):
                    if idx < len(values):
                        values[idx] += val

                for y in range(pos.y, pos.y + size.y):
                    for x in range(pos.x, pos.x + size.x):
                        # apply Floyd-Steinberg dithering
                        index = (y - pos.y) * size.y + (x - pos.x)

                        if values[index] > 0.5:
                            self.put(x, y, char)
                            error = values[index] - 1

                        else:
                            error = values[index]

                        add_safe(index + 1         , error * 7 / 16)
                        add_safe(index - 1 + size.x, error * 3 / 16)
                        add_safe(index     + size.x, error * 5 / 16)
                        add_safe(index + 1 + size.x, error * 1 / 16)

        self.alpha = alpha

    def put(self, x, y, char):
        if self.alpha > 0:
            self.quant_error += 1

            if self.quant_error >= 1 / self.alpha:
                self.target.put(x, y, char)
                self.quant_error -= 1 / self.alpha

    def here(self, char):
        self.put(self.pos.x, self.pos.y, char)

class CanvasTarget(object):
    def __init__(self, canvas, default):
        s = CanvasVector(canvas.size)
        
        self.canvas = canvas
        self.bitmap = [default for _ in range(s.y * s.x)]

    def row(self, y):
        return self.bitmap[y * self.canvas.size.x : (y+1) * self.canvas.size.x]

    def get(self, x, y):
        return self.bitmap[y * self.canvas.size.x + x]

    def put(self, x, y, char):
        x -= self.canvas.camera.x
        x += self.canvas.size.x / 2

        y -= self.canvas.camera.y
        y += self.canvas.size.y / 2

        x = int(x)
        y = int(y)

        if x < 0 or x >= self.canvas.size.x:
            return

        if y < 0 or y >= self.canvas.size.y:
            return

        idx = y * self.canvas.size.x + x
        self.bitmap[idx] = char
        
class CO_Rectangle(CanvasObject):
    def __init__(self, pos, size, char = '#', alpha = 1):
        self.pos = CanvasVector(pos)
        self.size = CanvasVector(size)
        self.alpha = alpha
        self.char = char

    def render(self, canvas, pen):
        pen.set_alpha(self.alpha)
        pen.move_to(self.pos)
        pen.rect(self.size, self.char)

class CO_Character(CanvasObject):
    def __init__(self, pos, char = '#', alpha = 1):
        self.pos = CanvasVector(pos)
        self.char = char
        self.alpha = alpha

    def render(self, canvas, pen):
        pen.set_alpha(self.alpha)
        pen.move_to(self.pos)
        pen.here(self.char)

class CO_Line(CanvasObject):
    def __init__(self, pos, pos2, char = '@', alpha = 1):
        self.pos = CanvasVector(pos)
        self.pos2 = CanvasVector(pos2)
        self.alpha = alpha
        self.char = char

    def render(self, canvas, pen):
        pen.set_alpha(self.alpha)
        pen.move_to(self.pos)
        pen.line_to(self.pos2, self.char)

class CO_Borders(CanvasObject):
    def __init__(self, char):
        self.char = char

    def render(self, canvas, pen):
        pen.set_alpha(1)
        
        cam = canvas.camera
        canvas.camera = CanvasVector((canvas.size.x / 2, canvas.size.y / 2))

        pen.move_to((0, 0))
        pen.line_to((canvas.size.x - 1, 0), self.char)
        pen.line_to((canvas.size.x - 1, canvas.size.y - 1), self.char)
        pen.line_to((0, canvas.size.y - 1), self.char)
        pen.line_to((0, 0), self.char)

        canvas.camera = cam


class CO_TextLine(CanvasObject):
    def __init__(self, pos, length = 50, text = '', borders = ('[', ']'), empty = '_', alpha = 1):
        self.pos = CanvasVector(pos)
        self.length = length
        self.borders = borders
        self.empty = empty
        self.alpha = alpha
        self.content = text

    def render(self, canvas, pen):
        pen.set_alpha(self.alpha)
        pen.move_to(self.pos)

        if self.borders[0]:
            pen.move((-1, 0))
            pen.here(self.borders[0])
            pen.move((1, 0))

        if self.content:
            pen.move_to(self.pos)
            pen.write(self.content[:self.length + 1])

        if len(self.content) < self.length:
            pen.line_toward((self.length + 1 - len(self.content), 0), self.empty)
        
        if self.borders[1]:
            pen.here(self.borders[1])

    def typed(self, text):
        self.content += text