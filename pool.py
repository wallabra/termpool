# -*- encoding: utf-8 -*-
import math
import struct
import traceback
import random
import time
import canvas
import io
import struct


class AudioChannel(object):
    def __init__(self):
        self._stream = io.BytesIO()

    def write(self, data):
        self._stream.write(data)

    def busy(self):
        return bool(len(self))

    def __len__(self):
        return len(self._stream.getvalue())

    def read(self, num_samples):
        import numpy as np

        l = min(num_samples, len(self))

        if l == 0:
            return []

        if l % 2 == 1:
            l -= 1

        self._stream.seek(0)
        data = self._stream.read(l)

        remaining = self._stream.read()
        del self._stream
        self._stream = io.BytesIO(remaining)

        return np.fromstring(data, dtype=np.int16)

class DummyAudioSegment(object):
    def __init__(self, name):
        self.name = name

class DummyAudioPlayer(object):
    def __init__(self, *args, **kwargs):
        super().__init__()

    def load_audio(self, fname):
        return DummyAudioSegment(fname)

    def read(self):
        return

    def play(self, data, volume = 1.0):
        pass

    def loop(self):
        pass

class AudioPlayer(object):
    def __init__(self, volume = 0.9, frame_rate = 48000, channels = 64, chunk_size = 3000):
        import pyaudio

        self.chunk_size = chunk_size
        self.frame_rate = frame_rate
        self.pya = pyaudio.PyAudio()
        self.output = self.pya.open(
            format=self.pya.get_format_from_width(width=2),
            channels=1,
            rate=self.frame_rate,
            output=True
        )
        self.streams = [AudioChannel() for _ in range(channels)]
        self.volume = volume

        self.cache = {}

    def load_audio(self, fname):
        from pydub import AudioSegment

        if fname in self.cache:
            return self.cache[fname]

        seg = AudioSegment.from_ogg(fname)

        seg.set_sample_width(2)
        seg.set_channels(1)
        seg.set_frame_rate(self.frame_rate)

        self.cache[fname] = seg.raw_data
        return seg.raw_data

    def read(self):
        import numpy as np

        lens = [len(s) for s in self.streams if s.busy()]

        if len(lens) == 0:
            return

        csize = min(self.chunk_size, *lens)
        chunks = [s.read(csize) for s in self.streams if s.busy()]
        lc = len(chunks)

        res = sum((c * self.volume / lc) for c in chunks).astype(np.int16)
        res = res.tobytes()

        return res

    def play(self, data, volume = 1.0):
        import numpy as np

        if volume != 1.0:
            data = (np.fromstring(data, dtype=np.int16) * volume).astype(np.int16).tobytes()

        for s in self.streams:
            if not s.busy():
                s.write(data)
                break

    def loop(self):
        ch = self.read()
        print('Channels playing: {}   '.format(len([c for c in self.streams if c.busy()])))

        if ch:
            self.output.write(ch)

try:
    import pydub
    import pyaudio
    import numpy

except ImportError:
    player = DummyAudioPlayer()

else:
    player = AudioPlayer()

SND_COLLISION1 = player.load_audio('sounds/collision1.ogg')
SND_COLLISION2 = player.load_audio('sounds/collision2.ogg')
SND_BOOST = player.load_audio('sounds/boost.ogg')
SND_SINK = player.load_audio('sounds/sink.ogg')


Vector = canvas.CanvasVector

class GameObject(canvas.CanvasObject):
    def render(self, canvas, pen):
        pass

    def tick(self, dtime):
        pass

    def event(self, event, *args, **kwargs):
        pass

    def remove(self):
        self.table.objects.remove(self)

class GameOverlay(canvas.OverlayObject, GameObject):
    pass

class PoolTable(canvas.CanvasObject):
    def __init__(self, radius):
        self.radius = canvas.CanvasVector(radius)

        self.time = time.time()
        self.fps = 0
        self.objects = []
        self.sunken = []
        self.middle = []
        self.overlays = []

        for _ in range(int(self.radius.x * self.radius.y / 9)):
            self.middle.append(canvas.CO_Character((
                random.randint(-self.radius.x, self.radius.x),
                random.randint(-self.radius.y, self.radius.y),
            ), random.choice(('.', ',', "'"))))

    def sank(self, ball):
        self.overlays.append(self.add_object(SinkOverlay, ball))
        self.sunken.append(ball)

    def add_ball(self, *args, **kwargs):
        # Backwards compatibility only.
        return self.add_object(PoolBall, *args, **kwargs)

    def add_object(self, cls, *args, **kwargs):
        obj = cls(self, *args, **kwargs)
        self.objects.append(obj)

        return obj

    def delta_time(self):
        t0 = self.time
        t1 = time.time()

        self.time = t1
        return t1 - t0

    def play_sound(self, source, sound, volume):
        player.play(sound, volume / (1 + math.sqrt((source.pos - Vector(self.canvas.camera)).size())))

    def tick(self, dtime):
        for obj in self.objects:
            obj.tick(dtime)

    def render(self, cv, pen):
        pen.set_alpha(1)

        dtime = self.delta_time()
        self.tick(dtime)

        def fps_counter(canvas, pen):
            cam = canvas.camera
            canvas.camera = Vector((canvas.size.x / 2, canvas.size.y / 2))

            self.fps = (int(1 / dtime) + self.fps * (1 - dtime)) / (2 - dtime)

            pen.move_to((3, 3))
            pen.write(' ' + str(int(self.fps)) + ' ')

            canvas.camera = cam

        return [
            # middle
            self.middle,

            # horizontal borders
            canvas.CO_Line((-self.radius.x, -self.radius.y), (self.radius.x, -self.radius.y), '%'),
            canvas.CO_Line((-self.radius.x, self.radius.y), (self.radius.x, self.radius.y), '%'),

            # vertical borders
            canvas.CO_Line((-self.radius.x, -self.radius.y), (-self.radius.x, self.radius.y), '$'),
            canvas.CO_Line((self.radius.x, -self.radius.y), (self.radius.x, self.radius.y), '$'),

            # corners
            canvas.CO_Character((-self.radius.x, -self.radius.y), '@'),
            canvas.CO_Character((self.radius.x, -self.radius.y), '@'),
            canvas.CO_Character((-self.radius.x, self.radius.y), '@'),
            canvas.CO_Character((self.radius.x, self.radius.y), '@'),
        ] + self.objects + [fps_counter]
        
class SinkOverlay(GameOverlay):
    def __init__(self, table, ball):
        self.table = table
        self.ball = ball

    def overlay_render(self, canvas, pen):
        pen.move_to((canvas.size.x - 4, canvas.size.y - 3 - self.table.overlays.index(self) * 2))
        self.ball.render_here(canvas, pen)

    def remove(self):
        super().remove()
        self.table.overlays.remove(self)

class PoolBall(GameObject):
    def __init__(self, table, pos, striped, char):
        self.table = table
        self.pos = Vector(pos)
        self.char = str(char)
        self.striped = striped
        
        self.solid = True
        self.velocity = Vector((0, 0))
    
    def check_collisions(self):
        if self.solid:
            if self.pos.x <= -self.table.radius.x or self.pos.x >= self.table.radius.x:
                self.velocity.x *= -0.9
                self.table.play_sound(self, SND_COLLISION2, self.velocity.size() / 100)
                
                if self.pos.x <= -self.table.radius.x:
                    self.pos.x = -self.table.radius.x + 1

                else:
                    self.pos.x = self.table.radius.x - 1

            if self.pos.y <= -self.table.radius.y or self.pos.y >= self.table.radius.y:
                self.velocity.y *= -0.9
                self.table.play_sound(self, SND_COLLISION2, self.velocity.size() / 100)

                if self.pos.y <= -self.table.radius.y:
                    self.pos.y = -self.table.radius.y + 1

                else:
                    self.pos.y = self.table.radius.y - 1

            for b2 in self.table.objects:
                if isinstance(b2, PoolBall):
                    if self is b2:
                        continue

                    if b2.solid and (self.pos - b2.pos).size() < 2:
                        self.collide(b2)
                        
    def collide(self, b2):
        b1 = self

        if (b1.pos == b2.pos):
            return

        b1_b2 = (b2.pos - b1.pos).unit()
        b2_b1 = (b1.pos - b2.pos).unit()

        v1 = b1.velocity
        v2 = b2.velocity

        p1 = v1.dot(b1_b2)
        p2 = v2.dot(b2_b1)

        va = b1_b2 * p1
        vb = b2_b1 * p2

        b2.velocity += va - vb
        b1.velocity += vb - va

        pos1 = b1.pos
        pos2 = b2.pos
        b1.pos = pos2 + b2_b1 * 2
        b2.pos = pos1 + b1_b2 * 2

        self.table.play_sound(b1, SND_COLLISION1, (p1 + p2) / 100)

    def update_pos(self, dtime):
        self.pos += self.velocity * dtime
        self.velocity /= 1 + (.05 * dtime)

    def render(self, canvas, pen):
        pen.move_to(self.pos)
        self.render_here(canvas, pen)

    def render_here(self, canvas, pen):
        pen.move((-1, 0))

        if self.striped:
            pen.here('{')

        else:
            pen.here('(')

        pen.move((1, 0))
        pen.here(self.char)
        pen.move((1, 0))

        if self.striped:
            pen.here('}')

        else:
            pen.here(')')

        pen.move((-1, -1))
        pen.here('_')
        pen.move((0, 2))
        pen.here(' ')

    def tick(self, dtime):
        self.update_pos(dtime)
        self.check_collisions()

class TickingTimer(GameObject):
    def __init__(self, table, delay, repeat = False):
        self.table = table
        self.delay = delay
        self.age = 0
        self._func = None
        self.repeat = repeat

    def __call__(self, func):
        self._func = func

    @classmethod
    def add(self, table, delay, func = None, repeat = False):
        tm = table.add_object(TickingTimer, delay, repeat)

        if func:
            tm(func)
            return tm

        else:
            def _decorator(self, func):
                tm(func)
                return func

            return _decorator

    def tick(self, dtime):
        if self._func:
            self.age += dtime

            if self.age >= self.delay:
                self._func()
                self.age = 0

                if not self.repeat:
                    self._func = None

class PlayerBall(PoolBall):
    def __init__(self, table, pos, char):
        self.table = table
        self.pos = Vector(pos)
        self.char = str(char)
        
        self.start_pos = Vector(pos)
        self.solid = True
        self.velocity = Vector((0, 0))
        self.keys = set()
    
    def key_down(self, key):
        self.keys.add(key)

    def key_up(self, key):
        self.keys.remove(key)

    def replace(self):
        self.pos = self.start_pos
        self.solid = True
        self.velocity = Vector((
            random.uniform(-15, 15),
            random.uniform(-15, 15),
        ))
        self.keys = set()

    def event(self, evt, *args, **kwargs):
        if evt == 'sink':
            TickingTimer.add(self.table, 2.5, self.replace)

    def tick(self, dtime):
        super().tick(dtime)

        if 'up' in self.keys:
            self.velocity += Vector((0, -2)) * dtime

        if 'down' in self.keys:
            self.velocity -= Vector((0, -2)) * dtime

        if 'right' in self.keys:
            self.velocity += Vector((2, 0)) * dtime

        if 'left' in self.keys:
            self.velocity -= Vector((2, 0)) * dtime

    def render_here(self, canvas, pen):
        pen.move((-1, 0))
        pen.here('<')

        pen.move((1, 0))
        pen.here(self.char)
        
        pen.move((1, 0))
        pen.here('>')

        pen.move((-1, -1))
        pen.here('_')
        
        pen.move((0, 2))
        pen.here(' ')

class PoolSink(GameObject):
    def __init__(self, table, pos, radius = 3):
        self.table = table
        self.pos = Vector(pos)
        self.radius = float(radius)
        self.inside = set()

    def render_pos(self):
        return canvas.CanvasVector((
            int(self.pos.x), int(self.pos.y)
        ))

    def render(self, canvas: canvas.Canvas, pen: canvas.CanvasPen):
        pen.set_alpha(1.0)

        pen.move_to(self.pos)

        for rad_i in range(0, int(self.radius * 2)):
            rad = rad_i / 2
            pen.angle = 0
            pen.move((0, -rad))
            pen.arc(rad, char = '*')
            pen.move_to(self.pos)

        pen.set_alpha(1.0)

        pen.angle = 0
        pen.move((0, -self.radius))
        pen.arc(self.radius, char = '@')

    def is_inside(self, ball: PoolBall):
        return (ball.pos - self.pos).size() < self.radius + 2

    def tick(self, dtime):
        ni = set(self.inside)

        for i in self.inside:
            if not self.is_inside(i):
                ni.remove(i)

                for o in self.table.overlays:
                    if o.ball is i:
                        o.remove()

                continue

            i.velocity *= Vector((0, 0))
            i.pos = self.pos
            i.solid = False

        if len(ni) < len(self.inside):
            del self.inside
            self.inside = ni
                
        for o in self.table.objects:
            if isinstance(o, PoolBall):
                if o in self.inside:
                    continue

                if self.is_inside(o):
                    self.table.sank(o)
                    self.inside.add(o)
                    self.table.play_sound(self, SND_SINK, 2)
                    
                    o.event('sink')
                    o.velocity *= Vector((0, 0))
                    o.pos = self.pos
                    o.solid = False

class Booster(GameObject):
    def __init__(self, table, pos, radius = 16, boost_amount = 10):
        self.table = table
        self.pos = Vector(pos)
        self.radius = float(radius)
        self.inside = set()
        self.boost_amount = boost_amount

    def render_pos(self):
        return canvas.CanvasVector((
            int(self.pos.x), int(self.pos.y)
        ))

    def render(self, canvas: canvas.Canvas, pen: canvas.CanvasPen):
        pen.set_alpha(1.0)

        pen.move_to(self.pos)

        for _ in range(int(math.pi * self.radius * self.radius / 8)):
            ang = random.uniform(0, math.pi * 2)
            dist = random.uniform(0, self.radius)
            pen.put(self.pos.x + math.cos(ang) * dist, self.pos.y + math.sin(ang) * dist, '~')

        pen.here('+')

        pen.angle = 0
        pen.move((0, -self.radius))

        pen.arc(self.radius, char = '+')
        pen.move_to(self.pos)

    def is_inside(self, ball: PoolBall):
        return (ball.pos - self.pos).size() < self.radius + 2

    def tick(self, dtime):
        new_inside = set(self.inside)

        for i in self.inside:
            if not self.is_inside(i):
                new_inside.remove(i)

            else:
                i.velocity *= 1 + self.boost_amount * dtime

        if len(new_inside) < len(self.inside):
            del self.inside
            self.inside = new_inside
                
        for o in self.table.objects:
            if isinstance(o, PoolBall):
                if o in self.inside:
                    continue

                if self.is_inside(o):
                    self.inside.add(o)
                    self.table.play_sound(self, SND_BOOST, 1.5)

if __name__ == '__main__':
    cv = canvas.Canvas((150, 35))
    cv.clear()

    table = PoolTable((90, 20))
    borders = canvas.CO_Borders('*')
    cv.add(table)
    cv.add(borders)

    gametime = 0
    old_time = time.time()

    try:
        # booster
        table.add_object(Booster, Vector((0, 0)), 6)

        # sinks
        table.add_object(PoolSink, Vector((-table.radius.x, -table.radius.y)))
        table.add_object(PoolSink, Vector((table.radius.x, -table.radius.y)))
        table.add_object(PoolSink, Vector((-table.radius.x, table.radius.y)))
        table.add_object(PoolSink, Vector((table.radius.x, table.radius.y)))

        # cue ball
        cue_ball = table.add_object(PlayerBall, (
            random.uniform(-table.radius.x / 3, table.radius.x / 3),
            random.uniform(-table.radius.y / 3, table.radius.y / 3),
        ), ' ')
        angle = random.uniform(0, math.pi * 2)

        speed = 200
        cue_ball.velocity = Vector((
            math.cos(angle) * speed,
            math.sin(angle) * speed
        ))

        pivot = Vector((
            random.randint(-table.radius.x, table.radius.x),
            random.randint(-table.radius.y, table.radius.y)
        ))

        def fling_pivot():
            global pivot

            ang = random.uniform(0, math.pi * 2)
            pivot += Vector((math.cos(ang) * 4, math.sin(ang) * 4))
    
        # smooth balls uwu
        for i in range(1, 8):
            table.add_ball(pivot, False, i)
            fling_pivot()

        # striped balls
        for i in range(1, 8):
            table.add_ball(pivot, True, i)
            fling_pivot()

        # eight ball
        table.add_ball((
            random.randint(-table.radius.x, table.radius.x),
            random.randint(-table.radius.y, table.radius.y)
        ), False, '8')

    except KeyboardInterrupt:
        cv.clear()
        cv.show_cursor()
        exit(0)

    except BaseException:
        cv.clear()
        with open('error.log', 'w') as el: el.write(traceback.format_exc())
        raise
        
    # render loop
    while True:
        try:
            new_time = time.time()
            delta_time = new_time - old_time

            gametime += delta_time

            cv.camera = cue_ball.pos
        
            cv.render()
            player.loop()

        except KeyboardInterrupt:
            cv.clear()
            cv.show_cursor()

            cv.swap()
            
            print("Cursor shown.")

            exit(0)

        except BaseException:
            cv.clear()

            with open('error.log', 'w') as el: el.write(traceback.format_exc())

            raise

